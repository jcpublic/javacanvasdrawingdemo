package com.example.canvasdrawingdemo;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // setup our system for drawing (get your tools ready!)
        // ------------------

        // 1. setup the frame
        ImageView imageView = findViewById(R.id.imageView);
        Bitmap b = Bitmap.createBitmap(300, 500, Bitmap.Config.ARGB_8888);

        // 2. setup the canvas
        Canvas canvas = new Canvas(b);

        // 3. setup your paintbrush
        Paint paintbrush = new Paint();


        // draw some stuff on the canvas
        // ------------------


        // --------------------
        // DRAW SOME LINES
        // --------------------

        // 1. Set the background color to black
        canvas.drawColor(Color.BLACK);

        // 2. Choose a YELLOW crayon!
        paintbrush.setColor(Color.YELLOW);

        // 3. draw a straight line (yellow)
        canvas.drawLine(10, 50, 200, 50, paintbrush);

        // 4. Change to a RED crayon!
        paintbrush.setColor(Color.GREEN);

        // 5. draw a diagonal line (red)
        canvas.drawLine(10, 50, 200, 150, paintbrush);


        // --------------------
        // DRAW SOME SQUARES
        // --------------------

        // 1. Change crayon color to white
        paintbrush.setColor(Color.WHITE);

        // 2. Draw a 20x20 square
        canvas.drawRect(100, 100, 120, 120, paintbrush);

        // 3. Draw a 50x50 square
        canvas.drawRect(150, 150, 200, 200, paintbrush);



        // --------------------
        // DRAW SOME SQUARES - TEST
        // --------------------

        // 1. Set the text size
        paintbrush.setTextSize(40);

        // 2. Draw text onto screen
        canvas.drawText("HELLO WORLD", 10, 400, paintbrush);

        // 3. Draw some more text
        paintbrush.setTextSize(10);
        paintbrush.setColor(Color.YELLOW);
        canvas.drawText("GOODBYE WORLD!", 10, 450, paintbrush);


        // put the canvas into the frame
        // ------------------
        imageView.setImageBitmap(b);


    }
}
